package assaf.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.div_btn).setOnClickListener(this);
		findViewById(R.id.mult_btn).setOnClickListener(this);
		findViewById(R.id.minus_btn).setOnClickListener(this);
		findViewById(R.id.plus_btn).setOnClickListener(this);
		findViewById(R.id.clear_btn).setOnClickListener(this);
	}
	
	public void onClick(View v){
		EditText firstNumber = (EditText)findViewById(R.id.first_number_txt);
		EditText secondNumber = (EditText)findViewById(R.id.second_number_txt);
		TextView result = (TextView)findViewById(R.id.result);
		float sum;
		if(v.getId()==R.id.plus_btn){
			try{
				sum = Float.parseFloat(firstNumber.getText().toString())+Float.parseFloat(secondNumber.getText().toString());
				result.setText(""+sum);
			}
			catch(NumberFormatException e){
			firstNumber.setText("");
			secondNumber.setText("");
			
			}
		}
		else if(v.getId()==R.id.minus_btn){
			try{
				sum = Float.parseFloat(firstNumber.getText().toString())-Float.parseFloat(secondNumber.getText().toString());
				result.setText(""+sum);

			}
			catch(NumberFormatException e){
			firstNumber.setText("");
			secondNumber.setText("");
			
			}
		}
		else if(v.getId()==R.id.mult_btn){
			try{
				sum = Float.parseFloat(firstNumber.getText().toString())*Float.parseFloat(secondNumber.getText().toString());
				result.setText(""+sum);

			}
			catch(NumberFormatException e){
			firstNumber.setText("");
			secondNumber.setText("");
			
			}
		}
		else if(v.getId()==R.id.div_btn){
			try{
				sum = Float.parseFloat(firstNumber.getText().toString())/Float.parseFloat(secondNumber.getText().toString());
				result.setText(""+sum);

			}
			catch(NumberFormatException e){
			firstNumber.setText("");
			secondNumber.setText("");
			
			}
		}
		if(v.getId()==R.id.clear_btn){
			firstNumber.setText("");
			secondNumber.setText("");
			result.setText("");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
